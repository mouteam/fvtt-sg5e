
export default class SG5eActorSheet extends dnd5e.applications.actor.ActorSheet5eCharacter {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.classes.push('sg5e-sheet')
        return options;
    }
}
