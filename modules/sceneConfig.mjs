import {Settings} from "./settings.mjs";

Hooks.on("renderSceneConfig", async (sheet, html, data) => {
    html.find(`nav[class="sheet-tabs tabs"]`).append(`
        <a class="item" data-tab="${Settings.id}">
            <i class="fas fa-dungeon"></i> ${game.i18n.localize("SG5E.shortTitle")}
        </a>
    `)

    const campaignTension = game.settings.get(Settings.id, Settings.gameSettings.campaignTensionDie);
    const sceneTension = data.document.getFlag(Settings.id, "sceneTensionDie") || "";

    const sceneConfigHtml = await renderTemplate(
        `${Settings.templatesPath}/sceneConfig.hbs`,
        {
            dataTab: Settings.id,
            tensionDiceOptions: Settings.id,
            sceneTension: sceneTension,
            campaignTension: campaignTension
        }
    );

    html.find(`div[data-tab="ambience"]`).after(sceneConfigHtml)
});
