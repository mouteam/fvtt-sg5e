export const Settings = {};

Settings.id = "sg5e"
Settings.rootPath = "/modules/" + Settings.id;
Settings.templatesPath = Settings.rootPath + "/templates";

Settings.tensionDice = {
    d4: "SG5E.settings.tensionDie.d4",
    d6: "SG5E.settings.tensionDie.d6",
    d8: "SG5E.settings.tensionDie.d8",
    d10: "SG5E.settings.tensionDie.d10",
    d12: "SG5E.settings.tensionDie.d12",
};

Settings.displayDifficulty = {}

Settings.gameSettings = {
    campaignTensionDie: 'campaignTensionDie',
    determinationPoints: 'determinationPoints',
    useGenericCurrency: 'useGenericCurrency'
};

export const registerSettings = function () {
    game.settings.register(Settings.id, Settings.gameSettings.campaignTensionDie, {
        name: "SG5E.settings.campaignTensionDie.name",
        hint: "SG5E.settings.campaignTensionDie.hint",
        scope: "world",
        type: String,
        choices: Settings.tensionDice,
        default: Settings.tensionDice.d6,
        config: true
    });

    game.settings.register(Settings.id, Settings.gameSettings.determinationPoints, {
        name: "SG5E.settings.determinationPoints.name",
        hint: "SG5E.settings.determinationPoints.hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
        requiresReload: true
    });

    game.settings.register(Settings.id, Settings.gameSettings.useGenericCurrency, {
        name: "SG5E.settings.useGenericCurrency.name",
        hint: "SG5E.settings.useGenericCurrency.hint",
        scope: "world",
        config: true,
        default: true,
        type: Boolean,
        requiresReload: true
    });

    console.info(`${Settings.id} | Registered Settings`);
}
