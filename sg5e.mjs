import "./modules/sceneConfig.mjs";

import {Settings, registerSettings} from "./modules/settings.mjs";
import SG5eActorSheet from "./modules/actorSheet.mjs";

Hooks.on("init", function () {
    console.log(`${Settings.id} | Initializing`);

    if (game.system.id !== "dnd5e") {
        return;
    }

    registerSettings()

    // Deletes unused skills
    delete dnd5e.config.skills.arc;
    delete dnd5e.config.skills.his;
    delete dnd5e.config.skills.rel;

    // Add news skills
    dnd5e.config.skills.cul = {label: "SG5E.SkillCul", ability: "wis"}
    dnd5e.config.skills.eng = {label: "SG5E.SkillEng", ability: "int"}
    dnd5e.config.skills.pil = {label: "SG5E.SkillPil", ability: "dex"}
    dnd5e.config.skills.sci = {label: "SG5E.SkillSci", ability: "int"}

    // languages
    dnd5e.config.languages = {
        common: "DND5E.LanguagesCommon",
        abydonian: "SG5E.LanguagesAbydonian",
        altairan: "SG5E.LanguagesAltairan",
        ancient: "SG5E.LanguagesAncient",
        aschen: "SG5E.LanguagesAschen",
        asgard: "SG5E.LanguagesAsgard",
        americanSign: "SG5E.LanguagesAmericanSign",
        ancientCanaanite: "SG5E.LanguagesAncientCanaanite",
        ancientCeltic: "SG5E.LanguagesAncientCeltic",
        ancientEgyptian: "SG5E.LanguagesAncientEgyptian",
        ancientGreek: "SG5E.LanguagesAncientGreek",
        ancientPhoenician: "SG5E.LanguagesAncientPhoenician",
        ancientSalish: "SG5E.LanguagesAncientSalish",
        arabic: "SG5E.LanguagesArabic",
        cuneiform: "SG5E.LanguagesCuneiform",
        czech: "SG5E.LanguagesCzech",
        english: "SG5E.LanguagesEnglish",
        euskara: "SG5E.LanguagesEuskara",
        farsi: "SG5E.LanguagesFarsi",
        french: "SG5E.LanguagesFrench",
        golap: "SG5E.LanguagesGolap",
        german: "SG5E.LanguagesGerman",
        greek: "SG5E.LanguagesGreek",
        hieroglyphs: "SG5E.LanguagesHieroglyphs",
        japanese: "SG5E.LanguagesJapanese",
        latin: "SG5E.LanguagesLatin",
        linearA: "SG5E.LanguagesLinearA",
        mandarin: "SG5E.LanguagesMandarin",
        mayan: "SG5E.LanguagesMayan",
        middleEnglish: "SG5E.LanguagesMiddleEnglish",
        norse: "SG5E.LanguagesNorse",
        ogham: "SG5E.LanguagesOgham",
        phoenician: "SG5E.LanguagesPhoenician",
        russian: "SG5E.LanguagesRussian",
        serbian: "SG5E.LanguagesSerbian",
        spanish: "SG5E.LanguagesSpanish",
        turkish: "SG5E.LanguagesTurkish",
        eldeoran: "SG5E.LanguagesEldeoran",
        furling: "SG5E.LanguagesFurling",
        genii: "SG5E.LanguagesGenii",
        goaUld: "SG5E.LanguagesGoaUld",
        hidomanSquid: "SG5E.LanguagesHidomanSquid",
        hittite: "SG5E.LanguagesHittite",
        kKaan: "SG5E.LanguagesKKaan",
        mostari: "SG5E.LanguagesMostari",
        nAhuatl: "SG5E.LanguagesNAhuatl",
        nakai: "SG5E.LanguagesNakai",
        netjerian: "SG5E.LanguagesNetjerian",
        novusEnglish: "SG5E.LanguagesNovusEnglish",
        nox: "SG5E.LanguagesNox",
        ohnes: "SG5E.LanguagesOhnes",
        oldSaxon: "SG5E.LanguagesOldSaxon",
        protoChinese: "SG5E.LanguagesProtoChinese",
        reetou: "SG5E.LanguagesReetou",
        retem: "SG5E.LanguagesRetem",
        rillaanian: "SG5E.LanguagesRillaanian",
        rokari: "SG5E.LanguagesRokari",
        samoye: "SG5E.LanguagesSamoye",
        shchizenya: "SG5E.LanguagesShchizenya",
        spirit: "SG5E.LanguagesSpirit",
        tahitian: "SG5E.LanguagesTahitian",
        teneez: "SG5E.LanguagesTeneez",
        terellan: "SG5E.LanguagesTerellan",
        thorn: "SG5E.LanguagesThorn",
        tollan: "SG5E.LanguagesTollan",
        unas: "SG5E.LanguagesUnas",
        universal: "SG5E.LanguagesUniversal",
        ursini: "SG5E.LanguagesUrsini",
        volsinii: "SG5E.LanguagesVolsinii",
        wraith: "SG5E.LanguagesWraith"
    };

    if (game.settings.get(Settings.id, Settings.gameSettings.useGenericCurrency)) {
        dnd5e.config.currencies = {
            currency: {
                label: "SG5E.Currency",
                abbreviation: "SG5E.CurrencyAbbr",
                conversion: 1
            }
        };
    }
});

Hooks.on("ready", function () {
    Actors.registerSheet(Settings.id, SG5eActorSheet, {
        types: ['character'],
        makeDefault: true,
        label: "SG5E.SheetClassCharacter"
    });
});
